package com.example.android_part_d_task_2;

/*

2. Создать список контактов. Каждый контакт содержит имя, email, адрес, телефон и иконку (или фотку) контакта.
Список содержит только иконку, имя и email контакта.
При нажатии на контакт - он должен открываться в новом activity с подробной о нем информацией.

*/

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_part_d_task_2.model.Person;
import com.example.android_part_d_task_2.utils.Helper;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_NAME = "extra_name";
    public static final String EXTRA_EMAIL = "extra_email";
    public static final String EXTRA_ADDRESS = "extra_address";
    public static final String EXTRA_PHONE_NUMBER = "extra_phoneNumber";
    public static final String EXTRA_AVATAR = "extra_avatar";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<Person> persons = Helper.generateListPerson();

        RecyclerView recyclerView = findViewById(R.id.am_persons_list);
        PersonAdapter personAdapter = new PersonAdapter(this, persons);
        recyclerView.setAdapter(personAdapter);

        personAdapter.setListener(new PersonAdapter.OnItemClickListener() {
            @Override
            public void onClick(Person person) {
                sendIntent(person);
            }
        });
    }

    //================================================================================//

    private void sendIntent(Person person){

        Intent intent = new Intent(this, PersonInfoActivity.class);

        intent.putExtra(EXTRA_NAME, person.getName());
        intent.putExtra(EXTRA_EMAIL, person.getEmail());
        intent.putExtra(EXTRA_ADDRESS, person.getAddress());
        intent.putExtra(EXTRA_PHONE_NUMBER, person.getPhoneNumber());

        int avatarImageId = this.getResources().getIdentifier(person.getAvatar(), "drawable", this.getPackageName());
        intent.putExtra(EXTRA_AVATAR, avatarImageId);

        startActivity(intent);
    }
}

