package com.example.android_part_d_task_2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_part_d_task_2.model.Person;

import java.util.List;

public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.PersonViewHolder> {

    private Context context;
    private List<Person> persons;
    private OnItemClickListener listener;

    public PersonAdapter(Context context, List<Person> persons) {
        this.context = context;
        this.persons = persons;
    }

    interface OnItemClickListener {
        void onClick(Person person);
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    //================================================================================//

    @NonNull
    @Override
    public PersonAdapter.PersonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

//      View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_person, parent, false);

//      Context context = parent.getContext();

        LayoutInflater inflater = LayoutInflater.from(context);
        View personView = inflater.inflate(R.layout.activity_person, parent, false);
        PersonViewHolder personViewHolder = new PersonViewHolder(personView);

//      return new PersonViewHolder(personView);
        return personViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PersonAdapter.PersonViewHolder holder, int position) {

        Person person = persons.get(position);

        ImageView avatar = holder.avatar;
        String avatarImage = person.getAvatar();

        int avatarImageId = context.getResources().getIdentifier(avatarImage, "drawable", context.getPackageName());

//        Drawable d = ResourcesCompat.getDrawable(context.getResources(), avatarImage, null);
//        Drawable d = ContextCompat.getDrawable(context, avatarImage);

        // If images are in
        // app/src/main/assets folder
/*
            InputStream inputStream = context.getAssets().open(avatarImage);
            Drawable d = Drawable.createFromStream(inputStream, null);
            avatar.setImageDrawable(d);
*/

        avatar.setImageResource(avatarImageId);

        TextView name = holder.name;
        name.setText(person.getName());

        TextView email = holder.email;
        email.setText(person.getEmail());
    }

    @Override
    public int getItemCount() {
        return persons.size();
    }

    //================================================================================//

    class PersonViewHolder extends RecyclerView.ViewHolder {

        private ImageView avatar;
        private TextView name;
        private TextView email;

        public PersonViewHolder(@NonNull View itemView) {
            super(itemView);

            avatar = itemView.findViewById(R.id.ap_avatar);
            name = itemView.findViewById(R.id.ap_name);
            email = itemView.findViewById(R.id.ap_email);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(persons.get(getAdapterPosition()));
                }
            });
        }
    }
}


