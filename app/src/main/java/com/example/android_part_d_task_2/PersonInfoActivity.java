package com.example.android_part_d_task_2;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import static com.example.android_part_d_task_2.MainActivity.EXTRA_ADDRESS;
import static com.example.android_part_d_task_2.MainActivity.EXTRA_AVATAR;
import static com.example.android_part_d_task_2.MainActivity.EXTRA_EMAIL;
import static com.example.android_part_d_task_2.MainActivity.EXTRA_NAME;
import static com.example.android_part_d_task_2.MainActivity.EXTRA_PHONE_NUMBER;

public class PersonInfoActivity extends AppCompatActivity {

    String name;
    String email;
    String address;
    String phoneNumber;
    int avatarImageId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_info);

        Intent intent = getIntent();

        name = intent.getStringExtra(EXTRA_NAME);
        email = intent.getStringExtra(EXTRA_EMAIL);
        address = intent.getStringExtra(EXTRA_ADDRESS);
        phoneNumber = intent.getStringExtra(EXTRA_PHONE_NUMBER);
        avatarImageId = intent.getIntExtra(EXTRA_AVATAR, -1);

        ImageView apiAvatarImage = findViewById(R.id.api_avatar);
        TextView apiName = findViewById(R.id.api_name);
        TextView apiEmail = findViewById(R.id.api_email);
        TextView apiAddress = findViewById(R.id.api_address);
        TextView apiPhoneNumber = findViewById(R.id.api_phone_number);


        apiAvatarImage.setImageResource(avatarImageId);
        apiName.setText(name);
        apiEmail.setText(email);
        apiAddress.setText(address);
        apiPhoneNumber.setText(phoneNumber);
    }
}

